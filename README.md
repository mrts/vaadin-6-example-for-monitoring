# Vaadin 6 example for monitoring

This is an example Vaadin 6 application that can be used for investigating how
the [Vaadin framework](https://vaadin.com/framework) works. Vaadin is a
single-page application framework based on [Google Web Toolkit](http://www.gwtproject.org/overview.html)
and the main challenge lies in how navigation in the UI works. Unlike ordinary
REST-based single-page applications, application view state changes are
rendered at server side and sent to the browser via *User Interface Definition
Language* messages, see [detailed explanation in the official Vaadin
book](https://vaadin.com/book/vaadin6/-/page/uidl.html) and *How does Vaadin
work?* in the [framework overview page](https://vaadin.com/framework).

**We need to be able to understand user actions and view state changes for
effective monitoring of our applications even though view state transitions
happen in e.g. button event handlers.**

## Acceptance test

Here is the acceptance test for good Vaadin app monitoring:

> Given that the application runs and monitoring is enabled  

> When I click on the *Customers* tab  
> Then I can see a corresponding user action event in a monitoring dashboard *Customers* section  
> And the response payload size

> When I click on the *Invoices* tab  
> Then I can see a corresponding user action event in a monitoring dashboard *Invoices* section  
> And the response payload size

> When I click on the *Exception* tab  
> And I click on the *Trigger exception* button  
> Then I can see a corresponding error event in a monitoring dashboard  
> And the user action that caused the error

> When I click on the *Vaadin error* tab  
> And I click on the *Trigger Vaadin error* button  
> Then I can see a corresponding error event in a monitoring dashboard  
> And the user action that caused the error

## Running the application

You need Maven and Java 8 JDK to build and run the application.

Execute the following command to build and run it:

    mvn package jetty:run

Then open http://localhost:8080/vaadin-6-example-for-monitoring/ in the browser
and follow the acceptance test above.

Alternatively, you can run the generated
`target/vaadin-6-example-for-monitoring-1.0.war` web application archive in any
Java application server (Tomcat, WildFly etc).

## Maven project

The application project was generated with the following Maven archetype:

    mvn archetype:generate \
        -DarchetypeGroupId=com.vaadin \
        -DarchetypeArtifactId=vaadin-archetype-clean \
        -DarchetypeVersion=1.7.2 \
        -DgroupId=com.vaadinmonitoring \
        -DartifactId=vaadin-6-example-for-monitoring \
        -Dversion=1.0 \
        -Dpackaging=war
