package com.vaadinmonitoring;

import com.vaadin.Application;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import java.util.Arrays;
import java.util.List;

/**
 * The Application's "main" class
 */
@SuppressWarnings("serial")
public class VaadinMonitoringTestingApplication extends Application {
    // @formatter:off
    private static final List<String[]> CUSTOMERS = Arrays.asList(
        new String[] { "Connie", "Estrada" },
        new String[] { "Leslie", "Cross" },
        new String[] { "Elmer", "Banks" },
        new String[] { "Julius", "Jordan" }
    );
    private static final List<String[]> INVOICES = Arrays.asList(
        new String[] { "1", String.join(" ", CUSTOMERS.get(0)), "$ 123" },
        new String[] { "2", String.join(" ", CUSTOMERS.get(1)), "$ 457" },
        new String[] { "3", String.join(" ", CUSTOMERS.get(2)), "$ 893" },
        new String[] { "4", String.join(" ", CUSTOMERS.get(3)), "$ 742" }
    );
    // @formatter:on

    private Window window;

    @Override
    public void init() {
        window = new Window("Vaadin application for testing monitoring");
        setMainWindow(window);
        window.addComponent(createTabSheet());
    }

    private TabSheet createTabSheet() {
        final TabSheet tabSheet = new TabSheet();
        tabSheet.setSizeFull();
        tabSheet.addTab(createTable(Arrays.asList("Firstname", "Lastname"), CUSTOMERS), "Customers");
        tabSheet.addTab(createTable(Arrays.asList("Number", "Customer", "Amount"), INVOICES), "Invoices");
        tabSheet.addTab(createExceptionButton(), "Exception");
        tabSheet.addTab(createVaadinErrorComponent(), "Vaadin error");
        return tabSheet;
    }

    private Table createTable(final List<String> columns, final List<String[]> rows) {
        final Table table = new Table();
        table.setSizeFull();
        for (final String column : columns) {
            table.addContainerProperty(column, String.class, "");
        }
        for (final String[] row : rows) {
            table.addItem(row, null);
        }
        return table;
    }

    private Button createExceptionButton() {
        final Button button = new Button("Trigger exception");
        button.addListener((Button.ClickListener) e -> {
            throw new RuntimeException("Example exception");
        });
        return button;
    }

    private Component createVaadinErrorComponent() {
        final VerticalLayout verticalLayout = new VerticalLayout();
        final Button button = new Button("Trigger Vaadin error");
        final Table table = createTable(Arrays.asList("Numbers"),
                Arrays.asList(new String[] { "1" }, new String[] { "2" }));

        button.addListener((Button.ClickListener) e -> {
            table.setCellStyleGenerator((o, o1) -> {
                throw new RuntimeException("Cause unrecoverable internal error in the Vaadin application");
            });
            table.requestRepaint();
        });
        verticalLayout.addComponent(button);
        verticalLayout.addComponent(table);
        return verticalLayout;
    }

}
